/*  BSD-License:

 Copyright (c) 2013 by Tobias Ilte, TH-Wildau, Germany
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 * Neither the name nicai-systems nor the names of its contributors may be
 used to endorse or promote products derived from this software without
 specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 @file		subMenues.h
 @brief		Shows the submenues and handles the input for them.
 @author	Tobias Ilte
 @date		14.11.2013
*/

#ifndef SUBMENUES_H_
#define SUBMENUES_H_

/**
 * Displays a submenu for requesting and displaying distance sensor data.
 */
void getNds3();

/**
 * Displays a submenu for requesting and displaying velocities of the wheels of the nibo-robot.
 */
void getVL();

/**
 * Displays a submenu for changing the mode of the nibo-robot.
 */
void changeMode();


void ledMenue();

void headlightMenue();

void distanceMenue();

void motorMenue();








void customMode();

void getDistances();




///transfer char-array, which contains the transferred data.
char glob_receivingData[9];

#endif /* SUBMENUES_H_ */
