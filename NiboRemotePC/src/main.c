/*  BSD-License:

 Copyright (c) 2013 by Tobias Ilte, TH-Wildau, Germany
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 * Neither the name nicai-systems nor the names of its contributors may be
 used to endorse or promote products derived from this software without
 specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 @file		main.c
 @brief		Handles initialization of the program and shows main menu.
 @author	Tobias Ilte
 @date		14.11.2013
 */

// required libraries
#include <stdlib.h>
#include <stdio.h>		// includes the standard i/o header file
#include <unistd.h> 	// unix-standard calls
#include "../include/subMenues.h"
#include "connectionService.h"
#include "../include/main.h"

//int glob_fd;

int main() {
	glob_fd = open_port();
	configure_port(glob_fd);
	printMainMenu();
	return 0;
}

void printMainMenu() {
	char choice = 0; //stores user-input

	while (1) {
		system("clear"); //clears the console
		printf("\n");
		printf("\t*****************************************************************\n");
		printf("\t*\t\t\t MAIN MENU \t\t\t\t*\n");
		printf("\t*\t\t\t------------\t\t\t\t*\n");
		printf("\t*\t\t\t\t\t\t\t\t*\n");
		printf("\t*\t\t1 - read NDS3\t\t\t\t*\n");
		printf("\t*\t\t2 - custom mode\t\t\t\t*\n");
		printf("\t*\t\t\t\t\t\t\t\t*\n");
		printf("\t*\t\tx - Quit\t\t\t\t*\n");
		printf("\t*\t\t\t\t\t\t\t\t*\n");
		printf("\t*****************************************************************\n\n");
		printf("\tYour Choice: ");
		while ((choice != '1') && (choice != 'x') && (choice != '2')) { //validate user-input
			choice = getchar();
		}

		// switch statement for the choice of the user
		switch (choice) {
			case '1':
				choice = 0; // sets choice to zero again, so there is no default value
				getNds3();
				break;
			case '2':
				choice = 0; // sets choice to zero again, so there is no default value
				customMode();
				break;
			case 'x':
				close(glob_fd); // close serial-port
				system("clear");
				exit(0);
		}
	}
}
