/*  BSD-License:

 Copyright (c) 2013 by Tobias Ilte, TH-Wildau, Germany
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 * Neither the name nicai-systems nor the names of its contributors may be
 used to endorse or promote products derived from this software without
 specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */


/**
 @file		subMenues.c
 @brief		Shows the submenues and handles the input for them.
 @author	Tobias Ilte
 @date		14.11.2013
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include "../include/subMenues.h"
#include "connectionService.h"
#include "niboProtocolParser.h"
#include "niboProtocolHelper.h"


void getNds3() {
	char choice = 0; //stores user-input
	while (1) {
		system("clear");
		printf("\n");
		printf("\tDISTANZSENSORMENUE\n");
		printf("\t------------------\n\n");
		registerFlags[NSPREG_DIST_NDS3] |= NSP_FLAG_GET;
		submitData();
		printf("Sensor NDS3:\t\t%3u \n\n", registerValues[NSPREG_DIST_NDS3]);
		printf("n - Distanzsensormesswerte aktualisieren\n");
		printf("z - Hauptmenue\n\n");
		printf("Ihre Auswahl: ");
		scanf("%c", &choice);
		while ((choice != 'n') && (choice != 'z')) { //validate user-input
			choice = getchar();
		}
		if (choice == 'n') {
			//nothing -> new loop pass -> new values
		} else if (choice == 'z') {
			break;
		}
	}
}

void customMode() {
	char choice = 0; //stores user-input
	while (1) {
		system("clear");
		printf("\n");
		printf("\tAuswahl\n");
		printf("\t----------\n\n");
		printf("2 - Scheinwerfer\n");
		printf("3 - LEDs\n");
		printf("4 - Motorsteuerung\n");
		printf("5 - Distanzsensoren\n\n");
		printf("z - Zurueck ins Hauptmenue\n\n");
		printf("Ihre Auswahl: ");
		scanf("%c", &choice);
		while ((choice != '2') && (choice != '3') && (choice != '4') && (choice != '5')
				&& (choice != 'z')) { //validate user-input
			choice = getchar();
		}

		if (choice == 'z')
			break;

		switch (choice) {
			case '2':
				headlightMenue();
				break;
			case '3':
				ledMenue();
				break;
			case '4':
				motorMenue();
				break;
			case '5':
				distanceMenue();
				break;
			case 'z':

				break;
			default:
				break;
		}
	}
}

void motorMenue() {
	char choice = 0; //stores user-input
	while (1) {
		system("clear");
		printf("\n");
		printf("\tMOTORMENUE\n");
		printf("\t------------------\n\n");
		registerFlags[NSPREG_MOTCURL] |= NSP_FLAG_GET;
		registerFlags[NSPREG_MOTCURR] |= NSP_FLAG_GET;
		registerFlags[NSPREG_ODOL] |= NSP_FLAG_GET;
		registerFlags[NSPREG_ODOR] |= NSP_FLAG_GET;

		submitData();

		printf("Motorspannung Links:\t\t%u \n", registerValues[NSPREG_MOTCURL]);
		printf("Motorspannung Rechts:\t\t%u \n", registerValues[NSPREG_MOTCURR]);
		printf("Geschwindigkeit Links:\t\t%u \n", registerValues[NSPREG_ODOL]);
		printf("Geschwindigkeit Rechts:\t\t%2u \n\n", registerValues[NSPREG_ODOR]);

		printf("n - neu anfordern; z - zurueck; s - stop; g - geschwindigkeit aendern:\n");

		while ((choice != 'z') && (choice != 'g') && (choice != 's')  && (choice != 'n')) { //validate user-input
			choice = getchar();
		}


		if (choice == 'n') {
			//nothing -> new loop pass -> new values
		} else if (choice == 'z') {
			break;
		}
	}
}

void distanceMenue() {
	char choice = 0; //stores user-input
	while (1) {
		system("clear");
		printf("\n");
		printf("\tDISTANZSENSORMENUE\n");
		printf("\t------------------\n\n");
		registerFlags[NSPREG_DIST_NDS3] |= NSP_FLAG_GET;
		registerFlags[NSPREG_DIST_L] |= NSP_FLAG_GET;
		registerFlags[NSPREG_DIST_FL] |= NSP_FLAG_GET;
		registerFlags[NSPREG_DIST_F] |= NSP_FLAG_GET;
		registerFlags[NSPREG_DIST_FR] |= NSP_FLAG_GET;
		registerFlags[NSPREG_DIST_R] |= NSP_FLAG_GET;
		submitData();

		printf("Sensor Links:\t\t%2u \n", registerValues[NSPREG_DIST_L]);
		printf("Sensor Vorne-Links:\t\t%2u \n", registerValues[NSPREG_DIST_FL]);
		printf("Sensor Vorne:\t\t%2u \n", registerValues[NSPREG_DIST_F]);
		printf("Sensor Vorne-Rechts:\t\t%2u \n", registerValues[NSPREG_DIST_FR]);
		printf("Sensor Rechts:\t\t%2u \n", registerValues[NSPREG_DIST_R]);
		printf("Sensor NDS3:\t\t%3u \n\n", registerValues[NSPREG_DIST_NDS3]);
		printf("n - Distanzsensormesswerte aktualisieren\n");
		printf("z - Hauptmenue\n\n");
		printf("Ihre Auswahl: ");
		scanf("%c", &choice);
		while ((choice != 'n') && (choice != 'z')) { //validate user-input
			choice = getchar();
		}
		if (choice == 'n') {
			//nothing -> new loop pass -> new values
		} else if (choice == 'z') {
			break;
		}
	}
}

void headlightMenue() {
	int brightness = -2;
	while (1) {
		system("clear");
		printf("\n");
		printf("\tScheinwerfer\n");
		printf("\t---------------------\n\n");
		printf("Zurueck zur Auswahl: -1\n");
		printf("Helligkeit(0-1024)\n");

		while ((brightness < -1) || (brightness > 1024)) {
			char garbage;
			do {
				garbage = getchar();
			} while (garbage != '\n');
			//fflush(stdin);
			//clearerr(stdin);
			scanf("%d", &brightness);
		}

		printf("test:%d\n", brightness);

		if (brightness == -1)
			break;

		leds_set_headlights(brightness);
		brightness = -2;
		submitData();
	}
}

void ledMenue() {
	char ledNumber = 0; //stores user-input
	char color = 0; //color of LED
	while (1) {
		system("clear");
		printf("\n");
		printf("\tLEDs\n");
		printf("\t----\n\n");
		printf("z - zurueck zur Auswahl\n");
		printf("LED-Nummer(0-7)\n");
		while ((ledNumber != '0') && (ledNumber != '1') && (ledNumber != '2') && (ledNumber != '3')
				&& (ledNumber != '4') && (ledNumber != '5') && (ledNumber != '6')
				&& (ledNumber != '7') && (ledNumber != 'z')) { //validate user-input
			ledNumber = getchar();
		}

		if (ledNumber == 'z')
			break;

		//validate user-input
		printf("Farbe(0-Aus; 1-Gruen; 2-Rot; 3-Orange):\n");
		while ((color != '0') && (color != '1') && (color != '2') && (color != '3')
				&& (color != 'z')) {
			color = getchar();
		}

		if (ledNumber == 'z')
			break;

		uint8_t i_ledNumber = ledNumber - '0';
		uint8_t i_color = color - '0';
		ledNumber = 0;
		color = 0;
		leds_set_status(i_color, i_ledNumber);
		submitData();

	}
}
